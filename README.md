# Dietitian Panel 
A project started by Richard Sinius to expand the creator's knowledge about web app development in Angular.
## What does it do?
The goal of this project is to come up with a dashboard-like SPA web application for dietitians, so that they can easily manage patients and their diets.
#### See it in action at: http://135.125.206.198:4100/
**E-mail:** dietitian@test.com
**Password:** test1234

**IMPORTANT!** The project doesn't use HTTPS protocol, because there is no back-end solution and no sensitive or personal data is being sent or stored in a database. Although this is the case, the identity of this project can still be very easily compromised. For your own safety DO NOT enter any sensitive/personal information in any input field in the project.
## Why Angular?
Just because the author of this project is passionate about creating **modern**, **robust** and **scalable** front-end solutions:)
##  The stack

 - Angular 13
 - Heavy use of Karma/Jasmine
 - [Json-server](https://github.com/typicode/json-server) / [json-server-auth](https://github.com/jeremyben/json-server-auth)
 - A bunch of Dockers on a VPS:)
