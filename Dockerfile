# Build
FROM node:16.10-alpine AS node
WORKDIR /usr/src/app
COPY package.json package-lock.json ./
RUN npm i
COPY . .
RUN npm run build

# Run
FROM nginx:latest
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=node /usr/src/app/dist/dietitian-panel /usr/share/nginx/html