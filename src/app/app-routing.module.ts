import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginGuard } from './core/service/guard/login.guard';
import { PanelGuard } from './core/service/guard/panel.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'login',
    loadChildren: () =>
      import('./feature/view/login/login.module').then(
        (m) => m.LoginViewModule
      ),
    canActivate: [LoginGuard],
  },
  {
    path: 'panel',
    loadChildren: () =>
      import('./feature/view/panel/panel.module').then((m) => m.PanelModule),
  },
  {
    path: 'work-in-progress',
    loadChildren: () =>
      import('./feature/view/work-in-progress/work-in-progress.module').then(
        (m) => m.WorkInProgressModule
      ),
    canActivate: [PanelGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
