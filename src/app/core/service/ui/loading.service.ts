import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class LoadingService {
  private state = new BehaviorSubject<boolean>(false);

  constructor() {
  }

  public setLoading(isLoading: boolean): void {
    this.state.next(isLoading);
  }

  public get isLoading(): boolean {
    return this.state.value;
  }
}
