import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { NotificationType } from '../../../shared/enum/notification-type.enum';

@Injectable({ providedIn: 'root' })
export class NotificationService {
  constructor(
    private snackBar: MatSnackBar,
    private translateService: TranslateService
  ) {}

  public showNotification(message: string, type: NotificationType): void {
    this.snackBar.open(this.translateService.instant(message), undefined, {
      duration: 4000,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
      panelClass: [type],
    });
  }
}
