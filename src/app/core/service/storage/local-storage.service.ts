import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  public get(key: string): any {
    const value = window.localStorage.getItem(key);

    if (value) {
      return JSON.parse(value);
    }

    return null;
  }

  public set(key: string, value: any): void {
    window.localStorage.setItem(key, JSON.stringify(value));
  }

  public clear(): void {
    window.localStorage.clear();
  }

  public remove(key: string): void {
    window.localStorage.removeItem(key);
  }

}
