import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { TokenService } from 'src/app/shared/class/http/token-service';
import { RouteName } from 'src/app/shared/enum/route-name.enum';

@Injectable({
  providedIn: 'root',
})
export class LogoutService {
  constructor(private tokenService: TokenService, private router: Router) {}

  public async logout(): Promise<void> {
    this.tokenService.clearToken();
    await this.router.navigate([RouteName.LOGIN]);
  }
}
