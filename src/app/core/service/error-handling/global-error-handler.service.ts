import { ErrorHandler, Injectable, NgZone } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ErrorType } from 'src/app/shared/enum/error-type.enum';
import { NotificationType } from 'src/app/shared/enum/notification-type.enum';
import { NotificationService } from '../ui/notification.service';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
  constructor(
    private notificationService: NotificationService,
    private translateService: TranslateService,
    private zone: NgZone
  ) {}

  public handleError(error: Error): void {
    this.zone.run(() => {
      if (error.name === ErrorType.USER_LEVEL_ERROR) {
        const errorMessage = error?.message ?? '';
        this.notificationService.showNotification(
          errorMessage?.length
            ? this.translateService.instant(error.message)
            : this.translateService.instant('UNDEFINED_ERROR'),
          NotificationType.NOTIFY_ERROR
        );
      }

      console.error(error);
    });
  }
}
