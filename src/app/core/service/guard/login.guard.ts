import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { RouteName } from '../../../shared/enum/route-name.enum';
import { TokenService } from '../../../shared/class/http/token-service';

@Injectable({ providedIn: 'root' })
export class LoginGuard implements CanActivate {
  constructor(private router: Router, private tokenService: TokenService) {}

  async canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean> {
    if (this.tokenService.hasTokenExpired()) {
      return true;
    }

    await this.router.navigate([RouteName.WORK_IN_PROGRESS]);
    return false;
  }
}
