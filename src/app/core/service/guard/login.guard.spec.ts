import { inject, TestBed } from '@angular/core/testing';
import {
  ActivatedRouteSnapshot,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TokenService } from '../../../shared/class/http/token-service';
import { RouteName } from '../../../shared/enum/route-name.enum';
import { LocalStorageService } from '../storage/local-storage.service';
import { LoginGuard } from './login.guard';
import createSpyObj = jasmine.createSpyObj;

describe('LoginGuard', () => {
  let service: LoginGuard;

  const jwtHelperServiceMock = createSpyObj('JwtHelperService', [
    'isTokenExpired',
  ]);
  const mockSnapshot = createSpyObj<RouterStateSnapshot>(
    'RouterStateSnapshot',
    ['toString']
  );
  const localStorageServiceMock = createSpyObj('LocalStorageService', [
    'get',
    'set',
    'clear',
    'remove',
  ]);
  const tokenServiceMock = createSpyObj<TokenService>('TokenService', [
    'getUserEmail',
    'saveToken',
    'clearToken',
    'hasTokenExpired',
  ]);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        LoginGuard,
        {
          provide: LocalStorageService,
          useValue: localStorageServiceMock,
        },
        {
          provide: JwtHelperService,
          useValue: jwtHelperServiceMock,
        },
        {
          provide: TokenService,
          useValue: tokenServiceMock,
        },
        {
          provide: RouterStateSnapshot,
          useValue: mockSnapshot,
        },
      ],
    });
    service = TestBed.inject(LoginGuard);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return true if token has expired', async () => {
    tokenServiceMock.hasTokenExpired.and.returnValue(true);
    const result = await service.canActivate(
      new ActivatedRouteSnapshot(),
      mockSnapshot
    );
    expect(result).toBeTrue();
  });

  it('should navigate to work-in-progress and return false if token is valid', inject(
    [Router],
    async (router: Router) => {
      tokenServiceMock.hasTokenExpired.and.returnValue(false);
      spyOn(router, 'navigate');
      const result = await service.canActivate(
        new ActivatedRouteSnapshot(),
        mockSnapshot
      );
      expect(result).toBeFalse();
      expect(router.navigate).toHaveBeenCalledWith([
        RouteName.WORK_IN_PROGRESS,
      ]);
    }
  ));
});
