import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { RouteName } from '../../../shared/enum/route-name.enum';
import { TokenService } from '../../../shared/class/http/token-service';
import { NotificationService } from '../ui/notification.service';
import { NotificationType } from 'src/app/shared/enum/notification-type.enum';

@Injectable({ providedIn: 'root' })
export class PanelGuard implements CanActivate {
  constructor(
    private router: Router,
    private tokenService: TokenService,
    private notificationService: NotificationService
  ) {}

  async canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean> {
    if (this.tokenService.hasTokenExpired()) {
      this.showInfoSnackbar();
      await this.router.navigate([RouteName.LOGIN]);
      return false;
    }

    return true;
  }

  private showInfoSnackbar(): void {
    this.notificationService.showNotification(
      'SESSION_TIMEOUT',
      NotificationType.NOTIFY_ERROR
    );
  }
}
