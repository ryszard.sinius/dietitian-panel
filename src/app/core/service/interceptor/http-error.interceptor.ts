import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LogoutService } from 'src/app/core/service/auth/logout.service';
import { UserLevelError } from 'src/app/shared/class/error-handling/user-level-error';
import { RouteName } from 'src/app/shared/enum/route-name.enum';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(private router: Router, private logoutService: LogoutService) {}

  public intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.status === 403 || error.status === 401) {
          this.accessDeniedAction();
          return throwError(() => new UserLevelError('ACCESS_DENIED'));
        }

        return throwError(() => error);
      })
    );
  }

  private async accessDeniedAction(): Promise<void> {
    this.logoutService.logout();
    await this.router.navigate([RouteName.LOGIN]);
  }
}
