import { Injectable } from '@angular/core';
import { RestHttpService } from './rest-http.service';
import { HttpMethod } from '../../../shared/enum/http-method.enum';
import { ApiEndpoint } from '../../../shared/enum/api-endpoint.enum';
import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class ModelService extends RestHttpService {
  public getAndDeserializeModel<T>(
    method: HttpMethod,
    api: ApiEndpoint,
    deserializationFunc: (httpResponse: HttpResponse<T>) => T,
    data?: any
  ): Observable<T> {
    return super[method](api, data ? data : []).pipe(
      map((httpResponse: HttpResponse<T>) => {
        return deserializationFunc(httpResponse);
      })
    );
  }
}
