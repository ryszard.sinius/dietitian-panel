import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UrlParam } from 'src/app/shared/interface/url-param.interface';
import { ApiEndpoint } from '../../../shared/enum/api-endpoint.enum';
import { RestUtilsService } from './rest-utils.service';

@Injectable({
  providedIn: 'root',
})
export class RestHttpService {
  constructor(
    private http: HttpClient,
    private restUtilsService: RestUtilsService
  ) {}

  public get(
    api: ApiEndpoint,
    parameters: UrlParam[] = []
  ): Observable<HttpResponse<any>> {
    return this.http.get<any>(
      this.restUtilsService.appendParametersToUrl(
        parameters,
        this.restUtilsService.buildUrl(api)
      ),
      { observe: 'response' }
    );
  }

  public post(api: ApiEndpoint, data: object): Observable<HttpResponse<any>> {
    return this.http.post<any>(this.restUtilsService.buildUrl(api), data, {
      observe: 'response',
    });
  }

  public put(api: ApiEndpoint, data: object): Observable<HttpResponse<any>> {
    return this.http.put<any>(this.restUtilsService.buildUrl(api), data, {
      observe: 'response',
    });
  }

  public delete(
    api: ApiEndpoint,
    parameters: UrlParam[] = []
  ): Observable<HttpResponse<any>> {
    return this.http.delete<any>(
      this.restUtilsService.appendParametersToUrl(
        parameters,
        this.restUtilsService.buildUrl(api)
      ),
      { observe: 'response' }
    );
  }
}
