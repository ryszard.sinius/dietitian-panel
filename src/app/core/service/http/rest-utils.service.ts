import { Injectable } from '@angular/core';
import { ApiEndpoint } from 'src/app/shared/enum/api-endpoint.enum';
import { UrlParam } from 'src/app/shared/interface/url-param.interface';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class RestUtilsService {
  public buildUrl(api: ApiEndpoint): string {
    return `${environment.url}${api}`;
  }

  public appendParametersToUrl(parameters: UrlParam[], url: string): string {
    let result = url;
    let firstParameter = true;

    for (const param of parameters) {
      result += this.buildUrlParameter(param, firstParameter);
      firstParameter = false;
    }

    return result;
  }

  private buildUrlParameter(
    urlParamOptions: UrlParam,
    isFirst: boolean
  ): string {
    return (
      (isFirst ? '?' : '&') +
      urlParamOptions.name +
      '=' +
      urlParamOptions.value?.toString()
    );
  }
}
