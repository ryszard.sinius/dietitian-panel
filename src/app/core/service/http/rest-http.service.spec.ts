import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RestHttpService } from './rest-http.service';

describe('RestHttpService', () => {
  let service: RestHttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [RestHttpService],
    });

    service = TestBed.inject(RestHttpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
