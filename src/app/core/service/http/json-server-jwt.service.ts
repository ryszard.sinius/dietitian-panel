import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TokenService } from '../../../shared/class/http/token-service';
import { LocalStorageService } from '../storage/local-storage.service';

@Injectable()
export class JsonServerJwtService extends TokenService {
  constructor(
    protected override storageService: LocalStorageService,
    protected override jwtHelperService: JwtHelperService
  ) {
    super(storageService, jwtHelperService);
  }

  protected extractToken(
    httpResponse: HttpResponse<any>
  ): string | null | undefined {
    return httpResponse?.body?.accessToken;
  }

  public getUserEmail(): string {
    return this.decodeToken()?.['email'] ?? '';
  }
}
