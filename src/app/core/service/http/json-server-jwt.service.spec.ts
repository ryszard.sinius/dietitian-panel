import { HttpResponse } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { JwtHelperService } from '@auth0/angular-jwt';
import { LocalStorageService } from '../storage/local-storage.service';
import { JsonServerJwtService } from './json-server-jwt.service';
import createSpyObj = jasmine.createSpyObj;
import SpyObj = jasmine.SpyObj;

describe('JsonServerJwtService', () => {
  let service: JsonServerJwtService;
  let localStorageServiceMock: SpyObj<LocalStorageService>;
  let jwtHelperServiceMock: SpyObj<JwtHelperService>;

  beforeEach(() => {
    localStorageServiceMock = createSpyObj<LocalStorageService>(
      'LocalStorageService',
      ['get', 'set', 'clear', 'remove']
    );
    jwtHelperServiceMock = createSpyObj<JwtHelperService>('JwtHelperService', [
      'isTokenExpired',
      'decodeToken',
    ]);

    TestBed.configureTestingModule({
      imports: [],
      providers: [
        JsonServerJwtService,
        {
          provide: LocalStorageService,
          useValue: localStorageServiceMock,
        },
        {
          provide: JwtHelperService,
          useValue: jwtHelperServiceMock,
        },
      ],
    });

    service = TestBed.inject(JsonServerJwtService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should extract token from HttpResponse body', () => {
    expect(() =>
      service.saveToken(
        new HttpResponse({
          body: {
            accessToken: 'token',
          },
        })
      )
    ).not.toThrow(new Error('TOKEN_EXTRACTION_ERROR'));
  });
});
