import { TestBed } from '@angular/core/testing';
import { RestUtilsService } from './rest-utils.service';
import { ApiEndpoint } from '../../../shared/enum/api-endpoint.enum';
import { environment } from 'src/environments/environment';

describe('RestUtilsService', () => {
  let service: RestUtilsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [RestUtilsService],
    });

    service = TestBed.inject(RestUtilsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should buid API url', () => {
    expect(service.buildUrl(ApiEndpoint.LOGIN)).toEqual(
      environment.url + ApiEndpoint.LOGIN
    );
  });

  it('should append parameters to URL', () => {
    expect(
      service.appendParametersToUrl(
        [
          {
            name: 'testParam1',
            value: 1,
          },
          {
            name: 'testParam2',
            value: 'someValue',
          },
        ],
        service.buildUrl(ApiEndpoint.CREATE_USER)
      )
    ).toEqual(
      service.buildUrl(ApiEndpoint.CREATE_USER) +
        '?testParam1=1&testParam2=someValue'
    );
  });

  it('should not append parameters to URL', () => {
    expect(
      service.appendParametersToUrl(
        [],
        service.buildUrl(ApiEndpoint.CREATE_USER)
      )
    ).toEqual(service.buildUrl(ApiEndpoint.CREATE_USER));
  });
});
