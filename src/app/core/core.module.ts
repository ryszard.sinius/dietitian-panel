import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TokenService } from '../shared/class/http/token-service';
import { LoadingSpinnerModule } from '../shared/module/loading-spinner/loading-spinner.module';
import { GlobalErrorHandler } from './service/error-handling/global-error-handler.service';
import { JsonServerJwtService } from './service/http/json-server-jwt.service';
import { HttpErrorInterceptor } from './service/interceptor/http-error.interceptor';
import { LoadingInterceptor } from './service/interceptor/loading.interceptor';
import { TokenInterceptor } from './service/interceptor/token.interceptor';
import { LocalStorageService } from './service/storage/local-storage.service';

@NgModule({
  declarations: [],
  imports: [LoadingSpinnerModule, MatSnackBarModule],
  exports: [LoadingSpinnerModule, MatSnackBarModule],
  providers: [
    JwtHelperService,
    {
      provide: ErrorHandler,
      useClass: GlobalErrorHandler,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoadingInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true,
    },
    {
      provide: TokenService,
      useClass: JsonServerJwtService,
      deps: [LocalStorageService, JwtHelperService],
    },
  ],
})
export class CoreModule {}
