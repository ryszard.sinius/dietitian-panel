import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpResponse } from '@angular/common/http';
import { TokenService } from 'src/app/shared/class/http/token-service';
import { LocalStorageService } from '../../service/storage/local-storage.service';

@Injectable()
export class JwtServiceStub extends TokenService {
  public constructor(
    protected override storageService: LocalStorageService,
    protected override jwtHelperService: JwtHelperService
  ) {
    super(storageService, jwtHelperService);
  }

  public override getToken(): string {
    return super.getToken();
  }

  public override clearToken(): void {
    super.clearToken();
  }

  public override hasTokenExpired(): boolean {
    return super.hasTokenExpired();
  }

  public getUserEmail(): string {
    return 'test@test.com';
  }

  public override decodeToken(): Record<string, any> | undefined | null {
    return super.decodeToken();
  }

  public override saveToken(httpResponse: HttpResponse<any>): void {
    return super.saveToken(httpResponse);
  }

  protected extractToken(
    httpResponse: HttpResponse<any>
  ): string | null | undefined {
    return null;
  }
}
