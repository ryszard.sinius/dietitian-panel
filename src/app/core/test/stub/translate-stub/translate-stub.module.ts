import { NgModule } from '@angular/core';
import { TranslatePipeStub } from './translate-pipe-stub';
import { TranslateService } from '@ngx-translate/core';
import { TranslateServiceStub } from './translate-service-stub';


@NgModule({
  declarations: [TranslatePipeStub],
  exports: [TranslatePipeStub],
  providers: [{
    provide: TranslateService,
    useClass: TranslateServiceStub
  }]
})
export class TranslateStubModule {
}
