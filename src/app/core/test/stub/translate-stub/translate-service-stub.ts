import { EventEmitter, Injectable } from '@angular/core';
import { of } from 'rxjs';

@Injectable()
export class TranslateServiceStub {
  public onLangChange = new EventEmitter();
  public onTranslationChange = new EventEmitter();
  public onDefaultLangChange = new EventEmitter();

  public addLangs(langs: string[]): void {
    return;
  }

  public getLangs(): string[] {
    return ['en'];
  }

  public getBrowserLang(): string {
    return '';
  }

  public getBrowserCultureLang(): string {
    return '';
  }

  public use(lang: string): null {
    return null;
  }

  public get(key: any): any {
    return of(key);
  }
}
