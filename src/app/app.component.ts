import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LoadingService } from './core/service/ui/loading.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor(
    public loadingService: LoadingService,
    private translateService: TranslateService
  ) {
    this.initTranslateService();
  }

  private initTranslateService(): void {
    this.translateService.use('en');
  }
}
