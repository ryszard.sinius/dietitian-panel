export interface Sort<T> {
  property: Extract<keyof T, string>;

  order: 'asc' | 'desc';
}
