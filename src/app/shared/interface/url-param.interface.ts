export interface UrlParam {
    name: string;
    value: any;
}