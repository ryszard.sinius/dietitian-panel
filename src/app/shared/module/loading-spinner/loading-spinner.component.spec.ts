import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingSpinnerComponent } from './loading-spinner.component';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

describe('LoadingSpinnerComponent', () => {
  let component: LoadingSpinnerComponent;
  let fixture: ComponentFixture<LoadingSpinnerComponent>;
  let debugElement: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LoadingSpinnerComponent],
      imports: [MatProgressSpinnerModule]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingSpinnerComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not show loading spinner if "show" is false', () => {
    component.show = false;
    fixture.detectChanges();
    expect(component.show).toBeFalsy();
    expect(debugElement.query(By.css('.container'))).toBeFalsy();
    expect(debugElement.nativeElement.querySelector('mat-spinner')).toBeFalsy();
  });

  it('should show loading spinner if "show" is true', () => {
    component.show = true;
    fixture.detectChanges();
    expect(component.show).toBeTruthy();
    expect(debugElement.query(By.css('.container'))).toBeTruthy();
    expect(debugElement.nativeElement.querySelector('mat-spinner')).toBeTruthy();
  });
});
