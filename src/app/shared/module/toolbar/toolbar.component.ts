import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToolbarComponent {
  @Output()
  public buttonClick: EventEmitter<void> = new EventEmitter();

  @Input()
  public buttonIconId = '';

  @Input()
  public titleIconId = '';

  public buttonClicked(): void {
    this.buttonClick.emit();
  }
}
