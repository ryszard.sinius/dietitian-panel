import { HttpResponse } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { JwtHelperService } from '@auth0/angular-jwt';
import { LocalStorageService } from 'src/app/core/service/storage/local-storage.service';
import { JwtServiceStub } from 'src/app/core/test/stub/jwt-service-stub';
import { UserLevelError } from '../error-handling/user-level-error';
import { TokenService } from './token-service';
import createSpyObj = jasmine.createSpyObj;
import SpyObj = jasmine.SpyObj;

describe('TokenService', () => {
  let service: TokenService;
  let localStorageServiceMock: SpyObj<LocalStorageService>;
  let jwtHelperServiceMock: SpyObj<JwtHelperService>;
  const mockTokenContainer = {
    token: 'Bearer testtoken',
  };

  beforeEach(() => {
    localStorageServiceMock = createSpyObj<LocalStorageService>(
      'LocalStorageService',
      ['get', 'set', 'clear', 'remove']
    );
    jwtHelperServiceMock = createSpyObj<JwtHelperService>('JwtHelperService', [
      'isTokenExpired',
      'decodeToken',
    ]);

    TestBed.configureTestingModule({
      imports: [],
      providers: [
        {
          provide: LocalStorageService,
          useValue: localStorageServiceMock,
        },
        {
          provide: JwtHelperService,
          useValue: jwtHelperServiceMock,
        },
      ],
    });

    service = new JwtServiceStub(localStorageServiceMock, jwtHelperServiceMock);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return token with prefix if in local storage', () => {
    localStorageServiceMock.get.and.returnValue(mockTokenContainer);
    expect(service.getToken()).toEqual(mockTokenContainer.token);
  });

  it('should return empty string if token not in local storage', () => {
    localStorageServiceMock.get.and.returnValue(undefined);
    expect(service.getToken()).toEqual('');
  });

  it('should clear saved token', () => {
    let removalResult = 0;
    localStorageServiceMock.remove.and.callFake((key: string) => {
      if (key === 'Authorization') {
        removalResult = 1;
      }
    });
    service.clearToken();
    expect(removalResult).toEqual(1);
  });

  it('should return true if token not found during expiration check', () => {
    localStorageServiceMock.get.and.returnValue(undefined);
    expect(service.hasTokenExpired()).toBeTrue();
  });

  it('should return true if token expired', () => {
    localStorageServiceMock.get.and.returnValue(mockTokenContainer);
    jwtHelperServiceMock.isTokenExpired.and.returnValue(true);
    expect(service.hasTokenExpired()).toBeTrue();
  });

  it('should return false if token not expired', () => {
    localStorageServiceMock.get.and.returnValue(mockTokenContainer);
    jwtHelperServiceMock.isTokenExpired.and.returnValue(false);
    expect(service.hasTokenExpired()).toBeFalse();
  });

  it('should return decoded user e-mail', () => {
    localStorageServiceMock.get.and.returnValue(mockTokenContainer);
    jwtHelperServiceMock.decodeToken.and.returnValue({ sub: 'test@test.com' });
    expect(service.getUserEmail()).toEqual('test@test.com');
  });

  it('should return undefined if failed to decode because token does not exist', () => {
    expect(service.decodeToken()).toEqual(undefined);
  });

  it('should return empty object if failed to decode', () => {
    localStorageServiceMock.get.and.returnValue(mockTokenContainer);
    jwtHelperServiceMock.decodeToken.and.returnValue(null);
    expect(service.decodeToken()).toEqual(null);
  });

  it('should throw UserLevelError with TOKEN_DECODE_ERROR if failed to save token', () => {
    expect(() => service.saveToken(new HttpResponse())).toThrow(
      new UserLevelError('TOKEN_EXTRACTION_ERROR')
    );
  });
});
