import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { LocalStorageService } from '../../../core/service/storage/local-storage.service';
import { JwtTokenContainer } from '../../interface/jwt-token-container.interface';

@Injectable()
export abstract class TokenService {
  protected static prefix = 'Bearer ';

  protected static authorizationHeader = 'Authorization';

  protected constructor(
    protected storageService: LocalStorageService,
    protected jwtHelperService: JwtHelperService
  ) {}

  public getToken(): string {
    const result: JwtTokenContainer = this.storageService.get(
      TokenService.authorizationHeader
    );
    return result ? result.token : '';
  }

  public clearToken(): void {
    this.storageService.remove(TokenService.authorizationHeader);
  }

  public hasTokenExpired(): boolean {
    const token = this.getToken();

    if (token) {
      return this.jwtHelperService.isTokenExpired(token);
    }

    return true;
  }

  public decodeToken(): Record<string, any> | undefined | null {
    return this.jwtHelperService.decodeToken(this.getToken());
  }

  public saveToken(httpResponse: HttpResponse<any>): void {
    const jwtToken = this.extractToken(httpResponse);

    if (jwtToken?.length) {
      this.storageService.set(TokenService.authorizationHeader, {
        token: jwtToken,
      });
    } else {
      throw new Error('TOKEN_EXTRACTION_ERROR');
    }
  }

  protected abstract extractToken(
    httpResponse: HttpResponse<any>
  ): string | null | undefined;

  public abstract getUserEmail(): string;
}
