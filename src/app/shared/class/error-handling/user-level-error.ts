import { ErrorType } from '../../enum/error-type.enum';

export class UserLevelError extends Error {
  constructor(message: string = '') {
    super(message);
    this.name = ErrorType.USER_LEVEL_ERROR;
  }
}
