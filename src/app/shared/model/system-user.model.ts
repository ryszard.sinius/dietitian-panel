import {BaseModel} from './base.model';

export class SystemUserModel extends BaseModel {
  id?: number;

  email?: string;

  name?: string;

  surname?: string;

  phoneNo?: string;
}
