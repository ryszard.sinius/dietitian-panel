export const enum RouteName {
  LOGIN = 'login',
  PANEL = 'panel',
  WORK_IN_PROGRESS = 'work-in-progress',
}
