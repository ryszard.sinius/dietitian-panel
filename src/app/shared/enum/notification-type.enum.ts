export const enum NotificationType {
  NOTIFY_SUCCESS = 'notification-success',
  NOTIFY_ERROR = 'notification-error'
}
