export const enum ApiEndpoint {
  LOGIN = 'login',
  USERS = 'api/users',
  CREATE_USER = 'api/users/create',
  UPDATE_USER = 'api/users/update',
  USER_TABLE_DATA = 'api/userTableData',
  DELETE_USER = 'api/users',
  USER_BIOMETRIC_DATA = 'api/biometrics/byUserId',
  BIOMETRICS_CREATE = 'api/biometrics/create',
  BIOMETRICS_UPDATE = 'api/biometrics/update'
}
