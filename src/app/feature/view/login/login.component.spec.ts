import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DebugElement } from '@angular/core';
import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { JwtHelperService } from '@auth0/angular-jwt';
import { of, throwError } from 'rxjs';
import { TranslateStubModule } from 'src/app/core/test/stub/translate-stub/translate-stub.module';

import { LoginViewModule } from './login.module';
import { LoginStoreService } from './service/store/login-store.service';
import { MatInputModule } from '@angular/material/input';
import SpyObj = jasmine.SpyObj;
import createSpyObj = jasmine.createSpyObj;
import { MatFormFieldModule } from '@angular/material/form-field';
import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let debugElement: DebugElement;
  let loginStoreServiceSpy: SpyObj<LoginStoreService>;
  let jwtHelperServiceMock = createSpyObj('JwtHelperService', [
    'isTokenExpired',
    'decodeToken',
  ]);
  let loginStoreServiceMock = createSpyObj('LoginStoreService', ['login']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        LoginViewModule,
        RouterTestingModule,
        MatSnackBarModule,
        HttpClientTestingModule,
        TranslateStubModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
      ],
      providers: [
        {
          provide: JwtHelperService,
          useValue: jwtHelperServiceMock,
        },
        {
          provide: LoginStoreService,
          useValue: loginStoreServiceMock,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
    loginStoreServiceSpy = TestBed.inject(
      LoginStoreService
    ) as SpyObj<LoginStoreService>;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show welcome message and navigate to work-in-progress on login success', inject(
    [Router],
    (mockRouter: Router) => {
      const routerSpy = spyOn(mockRouter, 'navigate');
      spyOn<any>(component, 'showWelcomeMessage');
      loginStoreServiceSpy.login.and.returnValue(of(new HttpResponse()));
      component.onCredentialsProvided({
        email: 'test@test.com',
        password: 'test123',
      });
      expect(component['showWelcomeMessage']).toHaveBeenCalledOnceWith();
      expect(routerSpy.calls.first().args[0]).toMatch(/.*work-in-progress.*/);
    }
  ));

  it('should show error message on login failure', () => {
    spyOn<any>(component, 'showLoginErrorMessage');
    loginStoreServiceSpy.login.and.returnValue(
      throwError(() => new HttpErrorResponse({}))
    );
    component.onCredentialsProvided({
      email: 'test@test.com',
      password: 'test123',
    });
    expect(component['showLoginErrorMessage']).toHaveBeenCalledOnceWith();
  });
});
