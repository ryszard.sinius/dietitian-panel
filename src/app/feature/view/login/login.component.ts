import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { take } from 'rxjs';
import { NotificationService } from 'src/app/core/service/ui/notification.service';
import { NotificationType } from 'src/app/shared/enum/notification-type.enum';
import { RouteName } from 'src/app/shared/enum/route-name.enum';
import { LoginCardSettings } from './interface/login-card-settings.interface';
import { UserCredentials } from './model/user-credentials.interface';
import { LoginStoreService } from './service/store/login-store.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  public loginCardSettings: LoginCardSettings = {
    submitButtonLabel: 'SUBMIT_BUTTON_LABEL',
    cardTitle: 'LOGIN_CARD_TITLE',
    emailLabel: 'EMAIL_INPUT_LABEL',
    emailPlaceholder: 'EMAIL_INPUT_PLACEHOLDER',
    invalidEmailError: 'INVALID_EMAIL_ERROR',
    emailRequiredError: 'EMAIL_REQUIRED_ERROR',
    passwordLabel: 'PASSWORD_INPUT_LABEL',
    passwordPlaceholder: 'PASSWORD_INPUT_PLACEHOLDER',
    passwordRequiredError: 'PASSWORD_REQUIRED_ERROR',
  };

  public constructor(
    private route: ActivatedRoute,
    private router: Router,
    private notificationService: NotificationService,
    private loginStoreService: LoginStoreService,
    private translateService: TranslateService
  ) {}

  private get welcomeMessage(): string {
    return this.translateService.instant('WELCOME_MESSAGE', {
      value: this.loginStoreService.userEmail,
    });
  }

  public onCredentialsProvided(userCredentials: UserCredentials): void {
    this.loginStoreService
      .login(userCredentials)
      .pipe(take(1))
      .subscribe(
        async () => {
          this.showWelcomeMessage();
          await this.router.navigate([RouteName.WORK_IN_PROGRESS], {
            relativeTo: this.route.root,
          });
        },
        (error: any) => {
          this.showLoginErrorMessage();
          throw error;
        }
      );
  }

  private showWelcomeMessage(): void {
    this.notificationService.showNotification(
      this.welcomeMessage,
      NotificationType.NOTIFY_SUCCESS
    );
  }

  private showLoginErrorMessage(): void {
    this.notificationService.showNotification(
      this.translateService.instant('LOGIN_FAILURE_MESSAGE'),
      NotificationType.NOTIFY_ERROR
    );
  }
}
