export interface LoginCardSettings {
  submitButtonLabel: string;
  cardTitle: string;
  emailLabel: string;
  emailPlaceholder: string;
  invalidEmailError: string;
  emailRequiredError: string;
  passwordLabel: string;
  passwordPlaceholder: string;
  passwordRequiredError: string;
}
