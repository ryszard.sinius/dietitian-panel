import { Injectable } from '@angular/core';
import { RestHttpService } from '../../../../../core/service/http/rest-http.service';
import { ApiEndpoint } from '../../../../../shared/enum/api-endpoint.enum';
import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { timeout } from 'rxjs/operators';
import { UserCredentials } from '../../model/user-credentials.interface';

@Injectable({ providedIn: 'root' })
export class LoginHttpService {
  public constructor(private restService: RestHttpService) {}

  public login(credentials: UserCredentials): Observable<HttpResponse<any>> {
    return this.restService
      .post(ApiEndpoint.LOGIN, credentials)
      .pipe(timeout(3000));
  }
}
