import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { LoginHttpService } from './login-http.service';
import { RestHttpService } from '../../../../../core/service/http/rest-http.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { delay } from 'rxjs/operators';
import { of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import SpyObj = jasmine.SpyObj;
import createSpyObj = jasmine.createSpyObj;

describe('LoginHttpService', () => {
  let service: LoginHttpService;
  let restHttpServiceSpy: SpyObj<RestHttpService>;
  const mockRestHttpService = createSpyObj('RestHttpService', ['post']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        LoginHttpService,
        {
          provide: RestHttpService,
          useValue: mockRestHttpService
        }
      ],
    });
    service = TestBed.inject(LoginHttpService);
    restHttpServiceSpy = TestBed.inject(RestHttpService) as SpyObj<RestHttpService>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should timeout after 3 seconds when logging in', fakeAsync(() => {
    let result: number;
    restHttpServiceSpy.post.and.returnValue(of(new HttpResponse({})).pipe(delay(10_000)));
    service.login({ email: 'test@test.com', password: 'test123' })
           .subscribe(() => result = 0, () => result = 1);
    tick(3000);
    // @ts-ignore
    expect(result).toEqual(1);
  }));
});
