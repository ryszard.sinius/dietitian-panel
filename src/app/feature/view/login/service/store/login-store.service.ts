import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginHttpService } from '../http/login-http.service';
import { HttpResponse } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { TokenService } from '../../../../../shared/class/http/token-service';
import { UserCredentials } from '../../model/user-credentials.interface';

@Injectable({ providedIn: 'root' })
export class LoginStoreService {
  public constructor(
    private loginHttpService: LoginHttpService,
    private userTokenService: TokenService
  ) {}

  public get userEmail(): string {
    return this.userTokenService.getUserEmail();
  }

  public login(credentials: UserCredentials): Observable<HttpResponse<any>> {
    return this.loginHttpService.login(credentials).pipe(
      tap(
        (response) => {
          try {
            this.userTokenService.saveToken(response);
          } catch (error) {
            throw error;
          }
        },
        () => {
          this.userTokenService.clearToken();
        }
      )
    );
  }
}
