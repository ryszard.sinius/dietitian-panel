import { TestBed } from '@angular/core/testing';

import { LoginStoreService } from './login-store.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LoginHttpService } from '../http/login-http.service';
import { of, throwError } from 'rxjs';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { catchError, take } from 'rxjs/operators';
import { TokenService } from '../../../../../shared/class/http/token-service';
import SpyObj = jasmine.SpyObj;
import createSpyObj = jasmine.createSpyObj;

describe('LoginStoreService', () => {
  let service: LoginStoreService;
  let loginHttpServiceSpy: SpyObj<LoginHttpService>;

  const loginHttpServiceMock = createSpyObj('LoginHttpService', ['login']);
  const tokenServiceMock = createSpyObj<TokenService>('TokenService', ['getUserEmail', 'saveToken',
    'clearToken']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      providers: [
        LoginStoreService,
        {
          provide: LoginHttpService,
          useValue: loginHttpServiceMock
        },
        {
          provide: TokenService,
          useValue: tokenServiceMock
        },
      ]
    });
    service = TestBed.inject(LoginStoreService);
    loginHttpServiceSpy = TestBed.inject(LoginHttpService) as SpyObj<LoginHttpService>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return user email as property', () => {
    tokenServiceMock.getUserEmail.and.returnValue('test@test.com');
    expect(service.userEmail).toEqual('test@test.com');
  });

  it('should save user token on successful login', () => {
    const mockResponse = new HttpResponse();
    loginHttpServiceSpy.login.and.returnValue(of(mockResponse));
    service.login({
      email: 'test@test.com',
      password: 'test'
    }).pipe(take(1)).subscribe();
    expect(tokenServiceMock.saveToken).toHaveBeenCalledOnceWith(mockResponse);
  });

  it('should clear user token on login failure', () => {
    loginHttpServiceSpy.login.and.returnValue(throwError(new HttpErrorResponse({})));
    service.login({
      email: 'test@test.com',
      password: 'test'
    }).pipe(
      take(1),
      catchError(() => of(null))
    ).subscribe();
    expect(tokenServiceMock.clearToken).toHaveBeenCalledOnceWith();
  });
});
