import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
} from '@angular/forms';
import { LoginCardSettings } from '../interface/login-card-settings.interface';
import { UserCredentials } from '../model/user-credentials.interface';

@Component({
  selector: 'app-login-card',
  templateUrl: './login-card.component.html',
  styleUrls: ['./login-card.component.scss'],
})
export class LoginCardComponent {
  @Input()
  public settings: LoginCardSettings | undefined;

  @Output()
  public credentials: EventEmitter<UserCredentials> = new EventEmitter<UserCredentials>();

  public form: FormGroup = this.formBuilder.group({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', Validators.required),
  });

  public constructor(private formBuilder: FormBuilder) {}

  public submit(): void {
    if (this.form.valid) {
      this.credentials.emit({
        email: this.form.get('email')?.value,
        password: this.form.get('password')?.value,
      });
    }
  }
}
