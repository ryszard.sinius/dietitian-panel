import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginCardComponent } from './login-card.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { TranslateStubModule } from 'src/app/core/test/stub/translate-stub/translate-stub.module';
import { LoginCardSettings } from '../interface/login-card-settings.interface';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';

describe('LoginCardComponent', () => {
  let component: LoginCardComponent;
  let fixture: ComponentFixture<LoginCardComponent>;
  let debugElement: DebugElement;
  const mockSettings: LoginCardSettings = {
    cardTitle: '',
    emailLabel: '',
    emailPlaceholder: '',
    emailRequiredError: '',
    invalidEmailError: '',
    passwordLabel: '',
    passwordPlaceholder: '',
    passwordRequiredError: '',
    submitButtonLabel: '',
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LoginCardComponent],
      imports: [
        BrowserAnimationsModule,
        TranslateStubModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatCardModule,
        MatInputModule,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginCardComponent);
    component = fixture.componentInstance;
    component.settings = mockSettings;
    debugElement = fixture.debugElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show error on invalid email', () => {
    component.form.patchValue({ email: 'test' });
    component.form.get('email')?.markAsTouched();
    fixture.detectChanges();
    expect(component.form.get('email')?.errors?.['email']).toBeTruthy();
    expect(debugElement.query(By.css('#invalid-email-error'))).toBeTruthy();
  });

  it('should show error on empty email', () => {
    component.form.get('email')?.markAsTouched();
    fixture.detectChanges();
    expect(component.form.get('email')?.errors?.['required']).toBeTruthy();
    expect(debugElement.query(By.css('#email-required-error'))).toBeTruthy();
  });

  it('should show error on empty password', () => {
    component.form.get('password')?.markAsTouched();
    fixture.detectChanges();
    expect(component.form.get('password')?.errors?.['required']).toBeTruthy();
    expect(debugElement.query(By.css('#password-required-error'))).toBeTruthy();
  });

  it('should not emit credentials when invalid', () => {
    spyOn(component.credentials, 'emit');
    component.submit();
    expect(component.credentials.emit).not.toHaveBeenCalled();
  });

  it('should emit valid credentials', () => {
    const mockCredentials = { email: 'test@test.com', password: 'test123' };
    component.form.setValue(mockCredentials);
    spyOn(component.credentials, 'emit');
    component.submit();
    expect(component.credentials.emit).toHaveBeenCalledWith(mockCredentials);
  });
});
