import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

import { WorkInProgressRoutingModule } from './work-in-progress-routing.module';
import { WorkInProgressComponent } from './work-in-progress.component';
import { NotificationComponent } from './notification/notification.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [WorkInProgressComponent, NotificationComponent],
  imports: [
    CommonModule,
    WorkInProgressRoutingModule,
    MatIconModule,
    TranslateModule.forChild({ extend: true }),
    MatButtonModule,
  ],
})
export class WorkInProgressModule {}
