import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { LogoutService } from 'src/app/core/service/auth/logout.service';
import { TranslateStubModule } from 'src/app/core/test/stub/translate-stub/translate-stub.module';
import { NotificationComponent } from './notification/notification.component';
import { WorkInProgressComponent } from './work-in-progress.component';
import SpyObj = jasmine.SpyObj;
import createSpyObj = jasmine.createSpyObj;


describe('WorkInProgressComponent', () => {
  let component: WorkInProgressComponent;
  let fixture: ComponentFixture<WorkInProgressComponent>;
  let mockLogoutService: SpyObj<LogoutService>;

  beforeEach(async () => {
    mockLogoutService = createSpyObj<LogoutService>('LogoutService', [
      'logout',
    ]);

    await TestBed.configureTestingModule({
      declarations: [WorkInProgressComponent, NotificationComponent],
      imports: [TranslateStubModule, MatIconModule, MatButtonModule],
      providers: [{ provide: LogoutService, useValue: mockLogoutService }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkInProgressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
