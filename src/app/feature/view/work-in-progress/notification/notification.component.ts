import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
})
export class NotificationComponent {
  @Input()
  public icon = '';

  @Input()
  public caption = '';

  @Input()
  public buttonCaption = '';

  @Output()
  public buttonClick: EventEmitter<void> = new EventEmitter();
}
