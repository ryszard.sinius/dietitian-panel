import { Component } from '@angular/core';
import { LogoutService } from 'src/app/core/service/auth/logout.service';

@Component({
  selector: 'app-work-in-progress',
  templateUrl: './work-in-progress.component.html',
  styleUrls: ['./work-in-progress.component.scss'],
})
export class WorkInProgressComponent {
  public constructor(public logoutService: LogoutService) {

  }


}
